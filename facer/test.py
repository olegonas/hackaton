import sys
sys.path.append('../')

from vk_tools.vk_search import VkParser
from vk_tools.vk_text_extractor import TextExtractor

from facer.picker import try_pick_person

def test():
    unknown_photo_url = 'https://pp.userapi.com/c604428/v604428197/255d9/xTF3vt2TzFM.jpg'
    test_name = 'Sergey Kurdyukov'
    parser = VkParser()
    response = parser.get_by_name(test_name)
    extractor = TextExtractor(None)

    users = extractor.get_users(response)
    print(users)

    for i in users:
        name = i['first_name'] + ' ' + i['last_name']
        known_photo_url = i['photo_max']

        is_found = try_pick_person(name, (known_photo_url, unknown_photo_url))

        if is_found:
            print(f'Got it, fuck yeah {known_photo_url}')

test()

