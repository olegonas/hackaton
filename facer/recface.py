import os

import face_recognition as fr

KNOWN_IMAGE_PATH = '../Images/known_images'
UNKNOWN_IMAGE_PATH = '../Images/unknown_images'

class Person:
    def __init__(self, name, known_image, unknown_image):
        self.name = name
        self.unknown_image = unknown_image
        self.unknown_encoding = None
        self.known_image = known_image
        self.known_encoding = None
        self.face_names = [name]

    def encoding_images(self):
        self.unknown_image = fr.load_image_file(self.unknown_image)
        self.known_image = fr.load_image_file(self.known_image)

        self.unknown_encoding = fr.face_encodings(self.unknown_image)[0]
        self.known_encoding = fr.face_encodings(self.known_image)[0]


    def face_detection(self):
        try:
            self.encoding_images()
            result = fr.compare_faces([self.known_encoding], self.unknown_encoding)
            return result
        except IndexError:
            print('Cant find face')
            return False

if __name__ == '__main__':
    person = Person('Mikhail Markov', None, None)
    print(person.face_detection())

