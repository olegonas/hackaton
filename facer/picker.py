import os
import requests

from facer.recface import Person
from vk_tools import *

def try_pick_person(name, image):
    known_image = images

    if not os.path.exists('tmp'):
        os.makedirs('tmp')

    if not os.path.exists(os.path.join('tmp', name)):
        os.makedirs(os.path.join('tmp', name))

    r = requests.get(known_image)
    known_name = known_image.split('/')[-1]
    known_path = os.path.join('tmp', name, known_name)
    if r.status_code == 200:
        with open(f'{known_path}', 'wb') as image:
            image.write(r.content)

    known_image_path = os.path.abspath(os.path.join(known_path))
    unknown_image_path = os.path.abspath('tmp/unknown_image.jpg')

    person = Person(name, known_image_path, unknown_image_path)

    return person.face_detection()

