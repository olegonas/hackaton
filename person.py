from typing import NamedTuple, List, Dict


class Person(NamedTuple):
    political_status: str
    age: int
    sex: int
    last_seen: int
    career: List[str]
    followers_count: int
    relation: int
    deactivated: bool

    @classmethod
    def from_dict(cls, json: Dict) -> 'Person':
        response = json['response'][0]
        try:
            age = 2018 - int(response['bdate'].split('.')[-1])
        except:
            age = None

        if 'personal' in response and 'political' in response['personal']:
            pol_status = response['personal']['political']
        else:
            pol_status = None

        if 'relation' in response:
            relation = response['relation']
        else:
            relation = None

        if 'followers_count' in response:
            flw_cnt = response['followers_count']
        else:
            flw_cnt = 0

        print(response)
        if 'last_seen' in response and 'time' in response['last_seen']:
            time = response['last_seen']['time']
        else:
            time = None

        if 'career' in response:

            career = response['career']
        else:
            career = []

        return cls(political_status=pol_status,
                   age=age,
                   sex=response['sex'],
                   relation=relation,
                   deactivated='deactivated' in response,
                   followers_count=flw_cnt,
                   last_seen=time,
                   career=career,
                   )
