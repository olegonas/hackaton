import json
from pprint import pprint
from urllib import request, parse
import re
from json import loads
from typing import NamedTuple, List, Dict
import requests

from .config import ID_URL, GROUP_URL, ACCESS_TOKEN


class Person(NamedTuple):
    name: str
    city: str
    groups_name: List[str]
    description: str
    photo_path: str

    @classmethod
    def from_dict(cls, fields: Dict) -> 'Person':
        pass


class VkParser:
    def __request(self, url, user_id) -> Dict:
        response = requests.get(url + str(user_id))
        return json.loads(response.text, encoding='utf-8')

    def get_by_id(self, user_id: int) -> Dict:
        serialized_response = self.__request(url=ID_URL, user_id=user_id)
        return serialized_response

    def get_by_name(self, name: str) -> Person:
        url = f'https://api.vk.com/method/users.search?q={name}&v=5.74&\
            count=40&offset=40&fields=photo_max&access_token={ACCESS_TOKEN}'
        response = requests.get(url)
        return json.loads(response.text, encoding='utf-8')

    def get_groups_by_id(self, user_id: int) -> Dict:
        serialized_response = self.__request(url=GROUP_URL, user_id=user_id)
        return serialized_response

    def fetch_themes(self, user_id: int) -> List[str]:
        wall_themes = requests.post("http://78.155.197.212:9999/get_result",
                                    json={"user_vk": user_id}).json()
        if 'interests' in wall_themes:
            return wall_themes['interests']
        else:
            return ['Интересы не обработаны ;(']


if __name__ == "__main__":
    parser = VkParser()
    # print(parser.get_by_id(6492))
    # print(parser.get_groups_by_id(6492))
    print(parser.get_by_name('Mikhail Markov'))
