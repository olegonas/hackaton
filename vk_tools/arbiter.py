import datetime
from person import Person

from vk_search import VkParser


class Arbiter:
    #  CREDIT SCORING BASED ON SOCIAL NETWORK DATA // Alexey A. MASYUTIN 2017 paper

    def get_scoring_fields(self, person: Person):
        scorepoint = 0
        if person.sex == 1:
            scorepoint += 27
        elif person.sex == 2:
            scorepoint += 15
        else:
            scorepoint += 10

        print(f'****{scorepoint}***')
        """
        1 — коммунистические;
        2 — социалистические;
        3 — умеренные;
        4 — либеральные;
        5 — консервативные;
        6 — монархические;
        7 — ультраконсервативные;
        8 — индифферентные;
        9 — либертарианские.
        """
        if person.political_status in {1, 2, 6}:
            scorepoint += 16
        elif person.political_status is None:
            scorepoint += 2
        else:
            scorepoint += 24

        print(f'****{scorepoint}***')

        """
        1 — не женат/не замужем;
        2 — есть друг/есть подруга;
        3 — помолвлен/помолвлена;
        4 — женат/замужем;
        5 — всё сложно;
        6 — в активном поиске;
        7 — влюблён/влюблена;
        8 — в гражданском браке;
        0 — не указано. 
        """
        if person.deactivated:
            scorepoint += 1
        else:
            scorepoint += 10

        print(f'****{scorepoint}***')

        if person.relation == 4:
            scorepoint += 27
        elif person.relation == 0:
            scorepoint += 23
        elif person.relation in {1, 6}:
            scorepoint += 19
        else:
            scorepoint += 16
        print(f'****{scorepoint}***')

        if len(person.career) > 1:
            scorepoint += 29
        else:
            scorepoint += 21
        print(f'****{scorepoint}***')

        try:
            last_seen_time = datetime.datetime.now() - datetime.datetime.fromtimestamp(
                person.last_seen)
            if last_seen_time.days < 1:
                scorepoint += 26
            elif last_seen_time.days < 3:
                scorepoint += 30
            elif last_seen_time.days < 37:
                scorepoint += 20
            elif last_seen_time.days < 149:
                scorepoint += 17
            elif last_seen_time.days >= 149:
                scorepoint += 11
            else:
                scorepoint += 5
            print(f'****{scorepoint}***')
        except:
            scorepoint += 2

        if person.age is None:
            scorepoint += 0
        else:
            if person.age < 25:
                scorepoint += 5
            elif person.age < 28:
                scorepoint += 12
            elif person.age < 37:
                scorepoint += 21
            elif person.age < 52:
                scorepoint += 44
            elif person.age >= 52:
                scorepoint += 66
            else:
                scorepoint += 3
        print(f'****{scorepoint}***')

        if person.followers_count < 10:
            scorepoint += 10
        elif person.followers_count < 100:
            scorepoint += 14
        elif person.followers_count < 1000:
            scorepoint += 20
        else:
            scorepoint += 28

        print(f'****{scorepoint}***')

        if person.deactivated is True:
            scorepoint -= 50
        print(f'****{scorepoint}***')

        return scorepoint


if __name__ == "__main__":
    pp = Person(political_status=9, age=34, sex=2, last_seen=1525359605,
                career=[
                    {'group_id': 22822305, 'country_id': 1, 'city_id': 2,
                     'from': 2006,
                     'until': 2014, 'position': 'Генеральный директор'},
                    {'company': 'Telegram', 'country_id': 65, 'city_id': 458,
                     'from': 2014,
                     'position': 'CEO'}], followers_count=6005865, relation=0,
                deactivated=False)

    zheka = Person(political_status=None, age=None, sex=2,
                   last_seen=1525568758, career=[], followers_count=0,
                   relation=0, deactivated=False)

    chuvak = Person(political_status=None, age=None, sex=1, last_seen=None,
                    career=[], followers_count=0, relation=None,
                    deactivated=True)

    arb = Arbiter()
    parser = VkParser()
    del_person = Person.from_dict(parser.get_by_id(136607969))
    print(del_person)
    print('chuvak score:', arb.get_scoring_fields(person=del_person))
