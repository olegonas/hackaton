# coding: utf-8

from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify

from vk_tools.vk_search import VkParser
from vk_tools.vk_text_extractor import TextExtractor

from facer.picker import try_pick_person

app = Flask(__name__)


@app.route("/")
def startServer():
    return render_template("Page.html")


# Разбор сообщения с фронта и передача на анализ
@app.route("/json", methods=['POST'])
def goDeeper():
    content = request.form
    print(content)
    file = request.files['Photo']
    print(file)
    result = analyze(content, file)
    if result:
        print("zdarova")
    return jsonify(result)


@app.route('/result', methods=['GET'])
def result():
    form = request.form
    result = 12313132131
    return render_template('result.html', r=result)


def analyze(content, file):
    file.save('facer/tmp/unknown_photo.jpg')
    parser = VkParser()
    response = parser.get_by_name(content["FIO"])
    extractor = TextExtractor(None)

    users = extractor.get_users(response)

    for i in users:
        name = i['first_name'] + ' ' + i['last_name']
        known_photo_url = i['photo_400_orig']

        is_found = try_pick_person(name, known_photo_url)

        if is_found:
            print(f'Got it, fuck yeah {known_photo_url}')
            return True
    return False
